-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 22 juin 2020 à 11:37
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `carrelet`
--

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(1, 'Cocktails classiques'),
(2, 'Mocktails'),
(3, 'Signatures'),
(4, 'Vasques'),
(5, 'Bocal'),
(6, 'Nourritures');

-- --------------------------------------------------------

--
-- Structure de la table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
CREATE TABLE IF NOT EXISTS `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20200221134656', '2020-02-21 13:47:34'),
('20200221140016', '2020-02-21 14:00:29'),
('20200221140953', '2020-02-21 14:10:02'),
('20200226155101', '2020-02-26 15:51:14'),
('20200227145350', '2020-02-27 14:54:06'),
('20200227145456', '2020-02-27 14:55:03'),
('20200227155434', '2020-02-27 15:54:44'),
('20200227155438', '2020-02-27 15:54:44'),
('20200302094010', '2020-03-02 09:40:26'),
('20200302101149', '2020-03-02 10:12:01'),
('20200302102048', '2020-03-02 10:20:56'),
('20200302110700', '2020-03-02 11:07:08'),
('20200302113905', '2020-03-03 08:54:13'),
('20200303085402', '2020-03-03 08:54:15'),
('20200303085858', '2020-03-03 08:59:10');

-- --------------------------------------------------------

--
-- Structure de la table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `recipe` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D34A04AD12469DE2` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `product`
--

INSERT INTO `product` (`id`, `name`, `price`, `recipe`, `category_id`, `active`, `image`) VALUES
(1, 'Gin Tonic', 8, 'Gin Beefeater -  Tonic', 1, 0, ''),
(2, 'Mojito', 8, 'Rhum havana 3\' - Jus de citron pressé - Menthe - Angostura bitter - Sucre cassonnade - Eau pétillante', 1, 0, ''),
(3, 'Caïpirinha', 8, 'Cachaça Aguacana - Jus de citron pressé - Sucre', 1, 0, ''),
(4, 'Maitai', 9, 'Rhum Havana  Especial Ambré - Cointreau - Jus de citron pressé - Sirop d\'orgeat', 1, 0, ''),
(5, 'Cosmopolitan', 9, 'Vodka Russian Standard - Cointreau - Jus de citron pressé - Jus de Cramberry', 1, 0, ''),
(6, 'Margarita', 9, 'Tequila Jose Cuervo Especial - Cointreau - Jus de citron pressé - Sucre de canne', 1, 0, NULL),
(7, 'Expresso Martini', 9, 'Vodka Russian standard - Liqueur de Kahua - Café - Sucre liquide', 1, 0, ''),
(8, 'Moscow Mule', 8, 'Vodka Russian standard - Citron pressé- Ginger beer', 1, 0, ''),
(9, 'Bloody Mary', 8, 'Vodka Russian standard - Jus de tomate  - Assaisonnement', 1, 0, ''),
(10, 'Southside Concombre', 9, 'Gin Beefeater - Citron - Sucre - Menthe - Concombre', 1, 0, ''),
(11, 'Sex On The Beach', 9, 'Vodka Russian standard - Liqueur de pêche - Jus de cramberry - Ananas pressé', 1, 0, ''),
(12, 'Pina Colada', 9, 'Rhum blanc - Rhum Havana Espcial Ambré- Crême de coco - Jus d\'ananas', 1, 0, ''),
(13, 'Cuba Libre', 8, 'Rhum ambré - Coca - Citron vert', 1, 0, ''),
(14, 'Old Fashioned', 10, 'Bouron - Sucre de cassonade - Angostura bitter', 1, 0, ''),
(15, 'Long Island', 13, 'Gin Beeteater - Vodka Russian standard - Rhum Havana 3\' - Tequila Jose Cuervo Especial - Cointreau - Citron pressé - Coca', 1, 0, ''),
(16, 'Love Strike', 12, 'Vodka Russian Standard - Liqueur de mandarin - Purée de fraise - Citron - Ginger Ale', 3, 0, ''),
(17, 'Galak', 12, 'Jack Daniel\'s - Liqueur de chocolat blanc - Liqueur mandarine - Angostura Bitter', 3, 1, ''),
(18, 'Vas\'y Francky', 12, 'Rhum épicé Gun\'s Bell - Liqueur de vanille - Purée de fruits de la passion - Jus d\'orange  - Menthe - Sucre de canne', 3, 0, ''),
(19, 'Ginger\'Ginger', 12, 'Gin Beefeater - Liqueur Pimm\'s n°1 - Citron pressé - Sirop de gimgembre -  Menthe - Bière', 3, 0, ''),
(20, 'T\'es qui là ?', 12, 'Tequila Jose Cuervo Especial - Crème de framboise - Citron pressé - Miel - Comcombre', 3, 0, ''),
(21, 'Detox', 6, 'Jus de pomme - Jus de citron pressé - Sirop de miel - Concombre - Eau pétillante', 2, 0, ''),
(22, 'Bébé Francly', 7, 'Jus de pomme - Orange pressé - Purée de passion - Menthe - Sirop de vanille', 2, 0, ''),
(23, 'Buongiorno', 7, 'Purée de fraise - Vinaigre balsamique - Jus de citron pressé - Sirop de poivre - Menthe - Eau pétillante', 2, 0, ''),
(24, 'Gin Tonic', 40, 'Gin Beefeater -  Tonic', 4, 0, ''),
(25, 'Cuba libre', 40, 'Rhum ambré - Coca - Citron vert', 4, 0, ''),
(26, 'Dark & Stormy', 40, 'Dark & Stormy', 4, 0, ''),
(27, 'Moscow Mule', 40, 'Vodka Russian standard - Citron pressé- Ginger beer', 4, 0, ''),
(28, 'Sex On The Beach', 40, 'Vodka Russian standard - Liqueur de pêche - Jus de cramberry - Ananas pressé', 4, 0, ''),
(29, 'Tequila / Vodka Sunrise', 40, 'Tequila / Vodka Sunrise', 4, 0, ''),
(50, 'Marinades du moment salées', 8, '(2pers).', 6, 0, ''),
(51, 'Fromages du moment', 8, '(2pers).', 6, 0, ''),
(52, 'Chiffonnade de charcuterie', 7, '(2pers).', 6, 0, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`roles`)),
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `username`, `roles`, `password`) VALUES
(1, 'Admin', '[\"ROLE_ADMIN\"]', '$argon2id$v=19$m=65536,t=4,p=1$VDR1dUtXdXhKd0taY0hLSQ$sCKrNbHSQNvaM1HcXaPLUVh6akJ9NYt+gdvzeUaevQE');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `FK_D34A04AD12469DE2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
