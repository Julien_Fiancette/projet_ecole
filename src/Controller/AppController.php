<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Product;
use App\Entity\Categories;
use App\Entity\Contact;
use App\Form\ContactType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;


class AppController extends AbstractController
{
   
    public function index()
    {
        return $this->render('app/index.html.twig');
    }

    public function carte()
    {
        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository(Categories::class)->findAll();
        return $this->render('app/carte.html.twig', [
            'categories' => $categories
        ]);
    }

    public function carte_details($id)
    {   
        $em = $this->getDoctrine()->getManager();
        $products = $em->getRepository(Product::class)->findBy(['category'=> $id]);
        $categorie = $em->getRepository(Categories::class)->findOneBy(['id'=> $id]);
        return $this->render('app/carte_details.html.twig', [
             'products' => $products, 'categorie' => $categorie
        ]);
    }

    public function evenements()
    {
        return $this->render('app/evenements.html.twig');
    }

    public function apropos()
    {
        return $this->render('app/apropos.html.twig');
    }

    public function contact(MailerInterface $mailer, Request $request)
    {
        
        $contact = New Contact();
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);


        if($form->isSubmitted() && $form->isValid()){
        
            $emaildata = $form->getData();

            $email = (new Email())
                ->from($emaildata->getEmail())
                ->to('Contact@lecarrelet.com')
                ->subject($emaildata->getName())
                ->html($emaildata->getMessage());

            $mailer->send($email);
            
            return $this->redirectToRoute('contact');
        }
        
        return $this->render('app/contact.html.twig',[
            'form' => $form->createView()
        ]);
    }

    public function cgu()
    {
        return $this->render('app/cgu.html.twig');
    }
}
